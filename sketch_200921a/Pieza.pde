class Pieza_Rompecabezas {  
  int num_pieza;
  float x_pos, y_pos;
 
 //Pieza_Rompecabezas
  Pieza_Rompecabezas(int pieza_n, float _xp, float _yp) {
    num_pieza = pieza_n;
    x_pos = _xp;
    y_pos = _yp;
  }  
  void onFlick(float _xp, float _yp) {
    x_pos = _xp;
    y_pos = _yp;
  }
  int NumeroPieza() {
    return num_pieza;
  }
  float xPosicion() {
    return x_pos;
  }
  float yPosicion() {
    return y_pos;
  }
  void draw() {
     noStroke();
    fill(col_principal);
    if (num_pieza == numero_cuadros-1) fill(col_fondo);   
    rect(x_pos+1, y_pos+1, tam_pieza2-1, tam_pieza2-1);
    if (num_pieza != numero_cuadros-1) {
      fill(0); 
      text(num_pieza+1, x_pos+tam_pieza2/2+1, 
           y_pos+tam_pieza2/2+textAscent()/2);
      fill(255);
      text(num_pieza+1, x_pos+tam_pieza2/2, 
           y_pos+tam_pieza2/2+textAscent()/2);
      stroke(col_borde1);
      //tamaños de sombras para bordes
      line(x_pos+tam_pieza2-1, y_pos+1, 
           x_pos+tam_pieza2-1, y_pos+tam_pieza2-1);
      line(x_pos+2, y_pos+tam_pieza2, 
           x_pos+tam_pieza2-1, y_pos+tam_pieza2); 
      stroke(col_borde2);
      line(x_pos+2, y_pos-1, 
           x_pos+2, y_pos+tam_pieza2); 
      line(x_pos+2, y_pos+1, 
           x_pos+tam_pieza2-1, y_pos+1); 
    }
  }
}
