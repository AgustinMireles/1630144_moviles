import android.view.MotionEvent;
import ketai.ui.*;
KetaiGesture gesture;
PImage photo;
PImage [] img = new PImage[9];
//numero de cuadros en el tablero
int numero_cuadros = 9; 

color col_principal = color(133, 110, 33);
color col_fondo = color(0, 0, 0);
color col_borde1 = color(255, 255, 255);
color col_borde2 = color(255, 255, 255);
int a, b, c, num_pieza, tam_pieza1, tam_pieza2;
 
 //Matriz de cuadros
Pieza_Rompecabezas[] pieza = new Pieza_Rompecabezas[numero_cuadros]; 
 
void setup() {  
  //tamaño de la ventana
  size(700, 700); 
  photo = loadImage("3.jpg");
  background(214, 214, 214);
  //sqrt(); devuelve la raíz cuadrada de un valor
  tam_pieza1 = int(sqrt(numero_cuadros));
  tam_pieza2 = width/tam_pieza1;
  textSize(tam_pieza2/2.7);
  textAlign(CENTER);
  
   for(int i=0;i<9;i++)
    img[i]=loadImage((i+1)+".jpg");
    
 
   //Establecer los valores x y y 
  PVector[] xy_datos = new PVector[numero_cuadros]; 
  
  // Valores de la celda
  for (int i = 0; i < numero_cuadros; i += tam_pieza1){ 
    for (int j = 0; j < tam_pieza1; j++) {
      xy_datos[a] = new PVector();
      xy_datos[a].x = j*tam_pieza2;
      xy_datos[a].y = b*tam_pieza2; 
      a++;
    }
    b++;
  } 
 
  int[] posicion = new int[numero_cuadros]; 
  // Coloca las piezas al azar
  for (int i = 0; i < numero_cuadros; i++) posicion[i] = 0;
  num_pieza = 0;
 
  while (num_pieza < numero_cuadros) {  
    //Acomodar piezas aleatoriamente 
    c = int(random(0, numero_cuadros));
    if (posicion[c] == 0) { 
      //evitar volver a dibujar la pieza.
      pieza[num_pieza] = new Pieza_Rompecabezas(num_pieza, 
           xy_datos[c].x, xy_datos[c].y); 
      // Creando  
      posicion[c] = 1;
      // Diseñar objeto
      pieza[num_pieza].draw(); 
      num_pieza++;
    }
  }
}
 
